const now = +new Date();
let index = 0;

// You can't use spread on a File object
export default (file: File, data: {}): MyUploadFile => {
  return {
    ...{
      lastModified: file.lastModified,
      name: file.name,
      originFileObj: file,
      size: file.size,
      type: file.type,
      uid: `rc-upload-${now}-${++index}`
    },
    ...data
  };
};
