import { Alert, Col, Modal, Row, Typography } from "antd";
import React, { PureComponent } from "react";

interface CCreateProps {
  albumId: string;
  secret: string;
  onClose: () => void;
}

export default class CCreated extends PureComponent<CCreateProps> {
  render() {
    return (
      <Modal
        title="Album credentials"
        visible
        onCancel={this.props.onClose}
        onOk={this.props.onClose}
      >
        <Alert
          description={
            <>
              <Row>
                <Col>
                  Secret:{" "}
                  <Typography.Text code copyable style={{ color: "black" }}>
                    {this.props.secret}
                  </Typography.Text>
                </Col>
              </Row>

              <Row>
                <Col>
                  Private link:{" "}
                  <Typography.Text code copyable style={{ color: "black" }}>
                    {`https://put.re/album/${this.props.albumId}/${this.props.secret}`}
                  </Typography.Text>
                </Col>
              </Row>

              <Row>
                <Col>
                  Public link:{" "}
                  <Typography.Text code copyable style={{ color: "black" }}>
                    {`https://put.re/album/${this.props.albumId}`}
                  </Typography.Text>
                </Col>
              </Row>
            </>
          }
          message={
            <Typography.Text strong style={{ color: "black" }}>
              Please, do store this securely.
            </Typography.Text>
          }
          type="info"
        />
      </Modal>
    );
  }
}
