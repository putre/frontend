import { Card, Checkbox, Icon, Popconfirm } from "antd";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import React, { PureComponent } from "react";

interface CCardProps {
  alt: string;
  description: JSX.Element;
  editable: boolean;
  title: JSX.Element;
  selected: boolean;
  src: string;
  onDelete: () => void;
  onSelectChange: (event: CheckboxChangeEvent) => void;
}

export default class CCard extends PureComponent<CCardProps> {
  render() {
    return (
      <Card
        actions={
          this.props.editable
            ? [
                <Popconfirm
                  cancelText="No"
                  icon={<Icon theme="twoTone" type="question-circle" />}
                  okText="Yes"
                  title="Delete this file?"
                  onConfirm={() => this.props.onDelete()}
                >
                  <Icon title="Delete" type="delete" />
                </Popconfirm>
              ]
            : []
        }
        cover={
          <a
            href={this.props.src}
            rel="noopener noreferrer"
            target="_blank"
            title={this.props.alt}
          >
            <div
              style={{
                backgroundImage: `url(${this.props.src})`,
                backgroundSize: "cover",
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                minHeight: 250,
                maxHeight: 250
              }}
            />
          </a>
        }
        extra={
          this.props.editable ? (
            <Checkbox
              checked={this.props.selected}
              onChange={this.props.onSelectChange}
            />
          ) : (
            undefined
          )
        }
        style={{ minHeight: this.props.editable ? 414 : 366, minWidth: 250 }}
      >
        <Card.Meta
          description={this.props.description}
          title={this.props.title}
        />
      </Card>
    );
  }
}
