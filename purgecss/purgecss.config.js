module.exports = {
  content: [
    "build/index.html",
    "build/static/js/*.js",
    "purgecss/rendered-*.html"
  ],
  css: ["build/static/css/*.css"],
  whitelistPatterns: [
    // Only add things that are hard to emulate here
    /^ant\-menu\-.*$/,
    /^ant\-modal\-mask\-hidden$/,
    /^ant\-popover.*/,
    /^ant\-tooltip.*/
  ]
};
