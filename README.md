# About

You are looking at the frontend code for the [put.re](https://put.re) website.

# Contributing

If you want to contribute features or fixes feel free to submit a pull request. For bigger changes it would be wise to email us first (<contact@put.re>). There might be things we leave out intentionally and nobody wants wasted work.

# Removed dependencies

We removed a few dependencies to reduce the bundle size. Take a look at `config-overrides.js` for more information.
